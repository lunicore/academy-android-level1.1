package se.lunicore.songninja.provided;

import java.util.ArrayList;

public class Songs {
    public static class Song {
        public String title;
        public String text;

        private Song(String title, String text) {
            this.title = title;
            this.text = text;
        }
    }

    public static ArrayList<Song> getSongs() {
        ArrayList<Song> list = new ArrayList<Song>();
        list.add(new Song("3-D", "3-D 3-D 3-D;\n3-D; 3-D\n3-D 3-D 3-D;\n3-D; 3-D\nEn extra dimension\n\n3-D 3-D 3-D;\n3-D; 3-D\n3-D 3-D 3-D;\n3-D; 3-D\nI vår television\n\n"));
        list.add(new Song("6-mästeriet", "Vi gingo med i 6-mästeriet\nVi jobba hårt där minst en gasque\nMen inte blev vi trötta av ett\nOch på vår tackfest fick vi plats\n\nMen utav sexets sprit blir människan till kropp & själ oskuldsfull & vit;\nmen utav sexets sprit blir människan till krop &"));
        list.add(new Song("75:an", "Våra tentor har vi flunkat\nVi har fått vår diagnos\nNu vår HB vi har dunkat\nVi ska hamna i hypnos\nDet gör inget om det luktar\nSom en flaska med klorin\nFör vi ska ju bara fukta\nVåra läppar med vårt vin\n\nMen sen är det slut på fina viner\nJa; nu ska"));
        list.add(new Song("800 samer", "Ja; varför tveka hur vi ska få el\nDom tål väl några Bequerel\nDet är ball med lysande renar\nVi tar plutonium från deras spenar\nEn lattjo pryl som Tjernobyl\nläks ju lätt med magnecyl\nDet är så kul med kärnenergi\nNu får dom hudallergi\n\n|: 800 samer;"));
        list.add(new Song("Abbe", "Abbe Abbe Abbe;\nAbbe; Abbe\nAbbe Abbe Abbe;\nAbbe; Abbe\nVar nollegeneral\n\nAbbe Abbe Abbe;\nAbbe; Abbe\nAbbe Abbe Abbe;\nAbbe; Abbe\nHan gjorde stor skandal\n\n"));
        list.add(new Song("Absolut", "Här har ni glada gänget som ska ut på galej\nVi nobbar ej en stilig tjej; en dans är helt okej\nDock finns det flera sköna saker i livet; det har vi förstått\nMan hittar det i glasen och det smakar rysligt gott\n\nDe&#039, e&#039, inte choklad eller Coca"));
        return list;
    }

}
