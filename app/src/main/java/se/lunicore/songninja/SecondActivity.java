package se.lunicore.songninja;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import se.lunicore.songninja.provided.Songs;

public class SecondActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second_layout);

        ViewPager vp = (ViewPager) findViewById(R.id.activity_second_viewpager);
        vp.setAdapter(new SongPagerAdapter(getSupportFragmentManager()));
    }

    /**
     * A simple pager adapter that represents songs from the songbook.
     */
    private class SongPagerAdapter extends FragmentStatePagerAdapter {
        private final ArrayList<Songs.Song> mSongs;

        public SongPagerAdapter(FragmentManager fm) {
            super(fm);
            mSongs = Songs.getSongs();
        }

        @Override
        public Fragment getItem(int position) {
            Songs.Song song = mSongs.get(position);
            return new SongPageFragment(song);
        }

        @Override
        public int getCount() {
            return mSongs.size();
        }
    }

    @SuppressLint("ValidFragment")
    public static class SongPageFragment extends Fragment {
        private final Songs.Song song;

        public SongPageFragment(Songs.Song song) {
            this.song = song;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_page_layout, container, false);

            TextView titleTextView = (TextView) rootView.findViewById(R.id.fragment_page_layout_title);
            titleTextView.setText(song.title);

            TextView songTextView = (TextView) rootView.findViewById(R.id.fragment_page_layout_songtext);
            songTextView.setText(song.text);

            return rootView;
        }
    }
}

