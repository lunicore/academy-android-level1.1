package se.lunicore.songninja;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity {

    private ArrayList<Integer> mColors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // What layout should be shown
        setContentView(R.layout.activity_main_layout);

        // @TODO 1. Context
        Activity activity = this;
        Context context = activity;

        // Get string from string.xml
        String subjectText = context.getString(R.string.dsek);

        TextView subject = (TextView) findViewById(R.id.main_activity_subject);
        subject.setText(subjectText);

        mColors = new ArrayList<Integer>();
        mColors.add(Color.rgb(252,63,63));
        mColors.add(Color.rgb(123,244,23));
        mColors.add(Color.rgb(20, 255, 36));
        mColors.add(Color.rgb(255, 20, 251));
        mColors.add(Color.rgb(255, 20, 0));
        mColors.add(Color.rgb(255, 20, 161));
        mColors.add(Color.rgb(255, 149, 20));
        mColors.add(Color.rgb(251, 255, 20));

        Button btn = (Button) findViewById(R.id.main_activity_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(startIntent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        /* Pick a random color from the list */
        int nbrColor = mColors.size();
        int randomIndex = new Random().nextInt(nbrColor);
        int myColor = mColors.get(randomIndex);

        TextView heart = (TextView) findViewById(R.id.main_activity_heart);
        heart.setTextColor(myColor);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mColors.clear();
    }
}
